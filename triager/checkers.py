"""Triager checkers."""
from collections import defaultdict

from triager import regexes, beaker
from settings import FAIL_KICKSTART, FAIL_WATCHDOG_EXPIRED, BEAKER_URL

from . import session


class TestFailureChecker:
    """TestFailureChecker Class."""

    check_functions = (
        'check_kickstart_error',
        'check_logs_with_regex',
        'check_beaker_watchdogs',
    )

    def __init__(self, pipeline, test_jobs):
        self.pipeline = pipeline
        self.test_jobs = test_jobs

    def check_kickstart_error(self):
        """
        Check job failed to Kickstart.

        If 'Boot test' has no duration, failed to provision.
        """
        failed_tests = []

        if self.pipeline.pipeline_id < 319106:
            # Pipelines older than this didn't have duration field.
            return []

        for testrun in self.test_jobs:
            if testrun.test.name == 'Boot test' and not testrun.duration:
                failed_tests.append(testrun)

        if failed_tests:
            return [(FAIL_KICKSTART, failed_tests)]

        return []

    def check_logs_with_regex(self):
        """Use regexes to find failures."""
        checker = regexes.RegexChecker()
        checker.download_lookups()

        failed_tests = defaultdict(list)

        for testrun in self.test_jobs:
            for log in testrun.logs:
                log_content = str(session.get(log.file).content)
                failure_id = checker.search(log_content, log, testrun)

                if failure_id:
                    failed_tests[failure_id].append(testrun)

        return list(failed_tests.items())

    def check_beaker_watchdogs(self):
        """Check if beaker watchdogs expired."""
        beaker_checker = beaker.BeakerChecker(BEAKER_URL)
        failed_tests = []
        for testrun in self.test_jobs:

            if not testrun.recipe_id:
                # Not a BeakerTestRun.
                continue

            if beaker_checker.check_testrun(testrun):
                failed_tests.append(testrun)

        if failed_tests:
            return [(FAIL_WATCHDOG_EXPIRED, failed_tests)]

        return []

    def __call__(self):
        """Run the checks."""
        failures = []
        for function in self.check_functions:
            result = getattr(TestFailureChecker, function)(self)
            if result:
                failures.extend(result)

        return failures

    def unidentified_tests(self, failures):
        """Return tests without a failure report."""
        not_found = []
        for testrun in self.test_jobs:
            found = False
            for _, failure_testruns in failures:
                if testrun in failure_testruns:
                    found |= True
            if not found:
                not_found.append(testrun)

        return not_found
