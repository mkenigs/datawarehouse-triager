"""Test triager."""
import unittest
import responses
from mock import patch

from datawarehouse import Datawarehouse

from triager.checkers import TestFailureChecker

from tests import mock_responses
from settings import FAIL_KICKSTART, FAIL_WATCHDOG_EXPIRED


DATAWAREHOUSE = Datawarehouse('http://server', None)


class TestTestFailureChecker(unittest.TestCase):
    """Test TestFailureChecker."""

    @responses.activate
    def test_check_kickstart_error(self):
        """Test check_kickstart_error."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    }
                ]
            }
        })
        pipeline = DATAWAREHOUSE.pipeline.get(517056)
        test_jobs = pipeline.failures.test.list()

        error, jobs = TestFailureChecker(pipeline, test_jobs).check_kickstart_error()[0]

        self.assertEqual(FAIL_KICKSTART, error)
        self.assertEqual(test_jobs, jobs)

    @responses.activate
    def test_check_kickstart_error_multiple(self):
        """Test check_kickstart_error. Multiple boot errors."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1235,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'aarch64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                ]
            }
        })

        pipeline = DATAWAREHOUSE.pipeline.get(517056)
        test_jobs = pipeline.failures.test.list()

        error, jobs = TestFailureChecker(pipeline, test_jobs).check_kickstart_error()[0]

        self.assertEqual(FAIL_KICKSTART, error)
        self.assertEqual(test_jobs, jobs)

    @responses.activate
    def test_check_kickstart_skipped(self):
        """Test check_kickstart_error skips old pipelines."""
        pipeline_319082 = mock_responses.pipeline_517056.copy()
        pipeline_319082['pipeline_id'] = 319082
        responses.add(responses.GET, 'http://server/api/1/pipeline/319082', json=pipeline_319082)
        responses.add(responses.GET, 'http://server/api/1/pipeline/319082/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                ]
            }
        })

        pipeline = DATAWAREHOUSE.pipeline.get(319082)
        test_jobs = pipeline.failures.test.list()

        errors = TestFailureChecker(pipeline, test_jobs).check_kickstart_error()

        self.assertEqual([], errors)

    @responses.activate
    def test_check_logs_with_regex(self):
        """Test check_logs_with_regex."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                            'universal_id': 'some_test',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [{'file': 'https://logs/console.log', 'name': 'console.log'}],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    }
                ]
            }
        })
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [mock_responses.lookup_issue_88]}})

        pipeline = DATAWAREHOUSE.pipeline.get(517056)
        test_jobs = pipeline.failures.test.list()

        error, jobs = TestFailureChecker(pipeline, test_jobs).check_logs_with_regex()[0]

        self.assertEqual(88, error)
        self.assertEqual(test_jobs, jobs)

        # Logs have no info.
        responses.replace(responses.GET, url='https://logs/console.log',
                          body=b'2019-11-17 07:15:09,105   ')

        errors = TestFailureChecker(pipeline, test_jobs).check_logs_with_regex()
        self.assertEqual([], errors)

    @responses.activate
    def test_check_logs_with_regex_returns_all_matches(self):
        """Test check_trace_with_regex. It should return all the failed tests."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                            'universal_id': 'some_test',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [{'file': 'https://logs/console.log', 'name': 'console.log'}],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1235,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                            'universal_id': 'some_test',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [{'file': 'https://logs/console.log', 'name': 'console.log'}],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    }
                ]
            }
        })
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [mock_responses.lookup_issue_88]}})

        pipeline = DATAWAREHOUSE.pipeline.get(517056)
        test_jobs = pipeline.failures.test.list()

        error, jobs = TestFailureChecker(pipeline, test_jobs).check_logs_with_regex()[0]

        self.assertEqual(88, error)
        self.assertEqual(test_jobs, jobs)

    @responses.activate
    def test_unidentified_tests(self):
        """Check unidentified_tests."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1235,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1236,
                        'test': {
                            'id': 1,
                            'name': 'Another test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                ]
            }
        })
        responses.add(responses.GET, 'http://server/api/1/issue/regex', json={'results': {'issue_regexes': []}})
        pipeline = DATAWAREHOUSE.pipeline.get(517056)
        test_jobs = pipeline.failures.test.list()

        checker = TestFailureChecker(pipeline, test_jobs)
        failures = checker()
        unidentified_tests = checker.unidentified_tests(failures)

        self.assertEqual([test_jobs[2]], unidentified_tests)

    @responses.activate
    def test_return_several_errors(self):
        """Check what happens with many different errors."""
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 100,
                    },
                    {
                        'id': 1235,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1236,
                        'test': {
                            'id': 1,
                            'name': 'Another test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [{'file': 'https://logs/console.log', 'name': 'console.log'}],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1237,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1238,
                        'test': {
                            'id': 1,
                            'name': 'Another test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [{'file': 'https://logs/console.log', 'name': 'console.log'}],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                    },
                    {
                        'id': 1239,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 100,
                    },
                ]
            }
        })
        responses.add(responses.GET, url='https://logs/console.log',
                      body=(b'[   41.451946] bnx2x 0045:01:00.0: Direct firmware load for '
                            b'bfq_bfqq_move '))
        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [mock_responses.lookup_issue_88]}})

        pipeline = DATAWAREHOUSE.pipeline.get(517056)
        test_jobs = pipeline.failures.test.list()

        checker = TestFailureChecker(pipeline, test_jobs)
        failures = checker()

        self.assertEqual(
            [
                (25, [test_jobs[1], test_jobs[3]]),
                (88, [test_jobs[2], test_jobs[4]]),
            ],
            failures
        )

        unidentified_tests = checker.unidentified_tests(failures)
        self.assertEqual(
            [test_jobs[0], test_jobs[5]],
            unidentified_tests
        )

    @responses.activate
    @patch('triager.beaker.BeakerChecker.check_testrun')
    def test_check_beaker_watchdogs(self, mock_check_testrun):
        """Test check_beaker_watchdogs."""
        mock_check_testrun.return_value = True
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056', json=mock_responses.pipeline_517056)
        responses.add(responses.GET, 'http://server/api/1/pipeline/517056/jobs/test/failures', json={
            'results': {
                'jobs': [
                    {
                        'id': 1234,
                        'test': {
                            'id': 1,
                            'name': 'Boot test',
                            'fetch_url': 'https://github.com/CKI-project/tests-beaker/' +
                                         'archive/master.zip#distribution/kpkginstall',
                        },
                        'waived': False,
                        'kernel_arch': {'name': 'x86_64'},
                        'kernel_debug': False,
                        'logs': [],
                        'invalid_result': False,
                        'untrusted': False,
                        'targeted': False,
                        'duration': 0,
                        'recipe_id': 123,
                        'task_id': 12,
                    }
                ]
            }
        })
        pipeline = DATAWAREHOUSE.pipeline.get(517056)
        test_jobs = pipeline.failures.test.list()

        failures = TestFailureChecker(pipeline, test_jobs).check_beaker_watchdogs()

        self.assertEqual(
            failures,
            [(FAIL_WATCHDOG_EXPIRED, test_jobs)]
        )


if __name__ == '__main__':
    unittest.main()
