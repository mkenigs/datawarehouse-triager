"""Test regexes."""
import os
import unittest
from dataclasses import dataclass
import responses

os.environ['DATAWAREHOUSE_URL'] = 'http://server'
from settings import NOT_FOUND  # noqa: E402 # pylint: disable=wrong-import-position
from triager import regexes  # noqa: E402 # pylint: disable=wrong-import-position


@dataclass
class LogMock:
    """Mock Log."""
    name: str


@dataclass
class TestMock:
    """Mock Test."""
    universal_id: str
    name: str


@dataclass
class TestRunMock:
    """Mock TestRun."""
    test: TestMock


class TestRegexChecker(unittest.TestCase):
    """Test TestRegexChecker."""

    def setUp(self):
        """setUp."""
        self.checker = regexes.RegexChecker()

    @responses.activate
    def test_search(self):
        """Test search."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('stress-ng', 'stress-ng'))
            )
        )

    @responses.activate
    def test_search_no_result(self):
        """Test search with no result."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                "Text with no matches.",
                LogMock('console.log'),
                TestRunMock(TestMock('stress-ng', 'stress-ng'))
            )
        )

    @responses.activate
    def test_search_test_name(self):
        """Test search with test_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": None,
            "test_name_match": "test-name"
        }

        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('stress-ng', 'stress-ng'))
            )
        )

        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('test-universal_id', 'Test with test-name in the name.'))
            )
        )

    @responses.activate
    def test_search_file_name(self):
        """Test search with file_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": 'file.name',
            "test_name_match": None
        }

        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('stress-ng', 'stress-ng'))
            )
        )

        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('test_file.name'),
                TestRunMock(TestMock('stress-ng', 'stress-ng'))
            )
        )

    @responses.activate
    def test_search_file_name_and_test_name(self):
        """Test search with file_name and test_name."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": 'file.name',
            "test_name_match": 'test-name'
        }

        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""

        # test_name and file_name wrong, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('stress-ng', 'stress-ng'))
            )
        )

        # test_name ok, file_name wrong, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('test-universal_id', 'Test with test-name in the name.'))
            )
        )

        # test_name wrong, file_name ok, text ok.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                text,
                LogMock('file.name'),
                TestRunMock(TestMock('stress-ng', 'stress-ng'))
            )
        )

        # test_name ok, file_name ok, text wrong.
        self.assertEqual(
            NOT_FOUND,
            self.checker.search(
                'wrong text',
                LogMock('file.name'),
                TestRunMock(TestMock('test-universal_id', 'Test with test-name in the name.'))
            )
        )

        # All ok.
        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('file.name'),
                TestRunMock(TestMock('test-universal_id', 'Test with test-name in the name.'))
            )
        )

    @responses.activate
    def test_search_regex_syntax(self):
        """Test search with some regex syntax."""
        lookup = {
            "id": 2,
            "issue": {
                "id": 64,
                "kind": {
                    "id": 1,
                    "description": "Kernel bug",
                    "tag": "Kernel Bug"
                },
                "description": "Bug description",
                "ticket_url": "https://bug.link",
                "resolved": False,
                "generic": False
            },
            "text_match": "Some weid string to look for 12345 !# -- ,,",
            "file_name_match": "some.*thing|console.log",
            "test_name_match": "this-name|other-name",
        }

        responses.add(responses.GET, 'http://server/api/1/issue/regex',
                      json={'results': {'issue_regexes': [lookup]}})

        self.checker.download_lookups()

        text = """
[   1234.0] Boot starting 123
[   1235.5] Some weid string to look for 12345 !# -- ,, !
"""
        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('this-name', 'this-name'))
            )
        )

        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('console.log'),
                TestRunMock(TestMock('other-name', 'other-name'))
            )
        )

        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('something'),
                TestRunMock(TestMock('other-name', 'other-name'))
            )
        )

        self.assertEqual(
            64,
            self.checker.search(
                text,
                LogMock('somebleblething'),
                TestRunMock(TestMock('other-name', 'other-name'))
            )
        )
