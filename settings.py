"""Settings file."""
import os

DATAWAREHOUSE_URL = os.environ.get('DATAWAREHOUSE_URL')
DATAWAREHOUSE_TOKEN = os.environ.get('DATAWAREHOUSE_TOKEN')
BEAKER_URL = os.environ.get('BEAKER_URL')

MESSAGE_QUEUE_NAME = os.environ.get('DATAWAREHOUSE_QUEUE_TRIAGER')
RABBITMQ_HOST = os.environ.get('DATAWAREHOUSE_RABBITMQ_HOST')
RABBITMQ_PORT = int(os.environ.get('DATAWAREHOUSE_RABBITMQ_PORT', 5672))
RABBITMQ_TIMEOUT_S = int(os.environ.get('RABBITMQ_TIMEOUT_S', 5))

# Failure id from the Datawarehouse.
FAIL_KICKSTART = 25
FAIL_WATCHDOG_EXPIRED = 36
UNIDENTIFIED_FAILURE = 57
NOT_FOUND = None
